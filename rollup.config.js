//import dts from "rollup-plugin-dts"

export default [
  {
    input: "./src/store.js",
    output: [
      { file: "dist/store.mjs", format: "es" },
      { file: "dist/store.umd.js", format: "umd", name: "svelte-proxied-store" },
    ]
  }
]
